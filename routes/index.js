var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
const userService = require('../users/user.service');
var User = require("../model/user.model");
let passwordHash = require('password-hash');

var sampleCol = require('../model/sample.model');

router.get('/', function (req, res) {
  User.getUsers(function (err, usersRes) {
    if (!err) {
      res.render('index', { title: "Index" });
    } else {
      console.log("err in get all users", err);
    }
  });
});

router.post('/login', authenticate);

function authenticate(req, res, next) {

  var UserMOdel = new User({
    username: req.body.username,
    password: req.body.password,
    role: req.body.role
  });

  userService.authenticate(UserMOdel.username, UserMOdel.password)
    .then(user => {
      console.log("user test*************", user);

      if (user) {
        UserMOdel['token'] = user.token;
        UserMOdel.save().then(response => {
          res.redirect('/home');
        }).catch(saveErr => {
          res.status(400).json({ message: "Error in token saving..." + saveErr });
        });
      } else {
        res.status(400).json({ message: "Sorry...Token Not Generated..." });
      }
    }).catch(err => {
      console.log("Errror............." + err);
    })
}

// function getAll(req, res, next) {
//   userService.getAll()
//     .then(users => res.json(users))
//     .catch(err => next(err));
// }

router.get('/home', function (req, res) {
  res.render('home', { title: "Index" });
});

module.exports = router;