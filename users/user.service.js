const config = require('../config.json');
const jwt = require('jsonwebtoken');
var User = require('../model/user.model');
var sampleCol = require('../model/sample.model');
// users hardcoded for simplicity, store in a db for production applications
const users = [{ id: 1, username: 'pooja', password: 'reset123#', role: 'Admin' }];

module.exports = {
    authenticate,
    //  getAll
};

async function authenticate(username, password) {
   // var matchedUser;
    var tokenKey;
    // sampleCol.getSamples(function (err, sampleUsers) {
    //     if (!err) {

    //         console.log("username*********" + sampleUsers[0].username + "poass" + sampleUsers[0].password);

    //         // for (let i of sampleUsers) {
    //         if (sampleUsers[0].username == username && sampleUsers[0].password == password) {
    //             console.log("username*********" + sampleUsers[0].username + "jkey" + sampleUsers[0].password);
    //             matchedUser = sampleUsers[0];
    //         }
    //         //  }

    //         if (matchedUser) {
    //             const token = jwt.sign({ sub: 1 }, config.secret);
    //             tokenKey = token;
    //             console.log("token*********" + token + "jkey" + tokenKey);

    //             const { password, userWithoutPassword } = matchedUser;
    //             return {
    //                 token
    //             };
    //         }
    //     } else {
    //         console.log("Error00000" + err);
    //     }
    // });

    const matchedUser = users.find(u => u.username === username && u.password === password);
    console.log("matchedUser", matchedUser);
    if (matchedUser) {
        const token = jwt.sign({ sub: matchedUser.id }, config.secret);
        const { password, userWithoutPassword } = matchedUser;
        return {
            token
        };
    }
}

// async function getAll() {

//     sampleCol.getSamples(function (err, sampleUsers) {
//         console.log("sampleUsers*********"+sampleUsers);
//         return sampleUsers.map(u => {
//             const { password, ...userWithoutPassword } = u;
//             console.log("passwod...", userWithoutPassword);
//             return userWithoutPassword;
//         });
//     });

    // return users.map(u => {
    //     const { password, userWithoutPassword } = u;
    //     return userWithoutPassword;
    // });
//}