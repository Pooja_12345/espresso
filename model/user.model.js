var mongoose = require('mongoose');
// Setup schema
var UserSchema = mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: false
    },
    role: {
        type: String,
        required: true
    }
});

// Export Contact model
var User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUsers = function (callback) {
    User.find(callback);
}

module.exports.deleteUsers = function (callback) {
    User.deleteMany(callback);
}