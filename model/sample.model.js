var mongoose = require('mongoose');
// Setup schema
var sampleSchema = mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: false
    },
    role: {
        type: String,
        required: true
    }
});
// Export Contact model
var sampleCol = module.exports = mongoose.model('sampleCol', sampleSchema);

module.exports.getSamples = function (callback) {

    sampleCol.find(callback);
    console.log("get samples mehtod");
}

module.exports.deleteSamples = function (callback) {
    sampleCol.deleteMany(callback);
}