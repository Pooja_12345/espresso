// server.js
// load the things we need
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
// Import Mongoose
let mongoose = require('mongoose');

var sampleCol = require('./model/sample.model');

var index = require('./routes/index');
var loginRout = require('./routes/login');
var homeRout = require('./routes/index');

// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://localhost/tokenDb', { useNewUrlParser: true });
var db = mongoose.connection;

// set the view engine to ejs
app.set('view engine', 'ejs');
// support parsing of application/json type post data
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

// use res.render to load up an ejs view file

app.use('/', index);
app.use('/login', index);
app.use('/home', homeRout);

app.get('/test', function (req, res) {
    testFunction1();
    testFuntion2();
    testFunction3();
});

app.listen(3000);
console.log('8080 is the magic port');

module.exports = app;